# Simple GUI experiments using Syndicate

This repository contains UI experiments using
[Syndicate](http://syndicate-lang.org/) and its
[OpenGL 2D support](https://github.com/tonyg/syndicate/tree/master/racket/syndicate-gl).

Files:

 - `gui.rkt`: Main entry point. Run `racket gui.rkt` to run the demo.

 - `layout/`: A simple widget layout engine, loosely inspired by TeX's boxes-and-glue model.

     - `sizing.rkt`: TeX-like "dimensions", including "fills"

     - `layout.rkt`: Uses "dimensions" to specify "table layouts",
       which are then realized in terms of specified rectangle
       coordinates

 - `hsv.rkt`: Utility for converting HSV colors to RGB.

Screenshot:

![Syndicate GUI screenshot](syndicate-gui-snapshot.png)
